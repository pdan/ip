var express = require('express')
var app = express()

app.get('/', function (req, res) {
  const remoteAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  res.send(remoteAddress.replace(/^.*:/, ''))
})

app.listen(3200, function () {
  console.log('Example app listening on port 3200!')
})
